import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursedemarerPage } from './coursedemarer.page';

describe('CoursedemarerPage', () => {
  let component: CoursedemarerPage;
  let fixture: ComponentFixture<CoursedemarerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursedemarerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursedemarerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
