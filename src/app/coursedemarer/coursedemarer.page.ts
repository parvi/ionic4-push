import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Platform, ToastController, LoadingController } from '@ionic/angular';
import { ActionSheetController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { ConnectivityService } from '../connectivity.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ExpeditionProvider } from '../api/expedition.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import * as firebase from 'Firebase';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';


declare var AdvancedGeolocation: any;
declare var google;

@Component({
  selector: 'app-coursedemarer',
  templateUrl: './coursedemarer.page.html',
  styleUrls: ['./coursedemarer.page.scss'],
})
export class CoursedemarerPage {
  telephonelivreur: string = '';
  @ViewChild('map') mapElement: ElementRef;
  infos = [];
  ref: any;
  telephoneExpediteur: string = '';
  currentLat: any = null;
  currentLng: any = null;
  marker: any
  icon: string;
  nom: string = '';
  directionsService: any;
  directionsDisplay: any;
  adresse: string = '';
  adresseExpediteur: string = '';
  positionNull: boolean = false;
  un: boolean = false;
  deux: boolean = false;
  trois: boolean = false;
  quatre: boolean = false;
  cinq: boolean = false;
  nomDestinataire: string = '';
  livreur: string = '';
  isTracking: boolean = false;
  map: any;
  mapInitialised: boolean = false;
  apiKey: any;
  mapshow: boolean = false;
  etat: boolean;
  trackedRoute: any;
  currentMapTrack = null;
  positionSubscription: Subscription;
  showSpinner: boolean = true;
  showrecuperer: boolean;
  showcloture: boolean;
  previousTracks = [];
  affectation: any;
  sblistt: any;
  locationCoords: any;
  progress: number = 0.0;
  mappres:boolean=false;
  constructor(private platform: Platform,
    public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    public connectivityService: ConnectivityService,
    private geo: Geolocation,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private loadingController: LoadingController,
    private servicesExpedition: ExpeditionProvider,
    private callNumber: CallNumber,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy, ) {
    /* this.storage.get("user").then((user) => {
       this.ref = firebase.database().ref(user.telephone + '/');
       this.ref.on('value', resp => {
         this.infos = [];
         this.infos = snapshotToArray(resp);
       });
     });*/
    this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: ""
    }
    this.checkGPSPermission();
    //this.getLocationCoordinates();
    this.loadGoogleMaps();
    this.initvar();
    this.storage.get("progresse").then(vl => {
      if (vl) {
        this.progress = vl;
      }
    })

  }
  saveInfo(coordonnes: any) {
    this.storage.get("user").then((user) => {

      console.log(user)
      let newInfo = firebase.database().ref(user.telephone + '/').push();
      newInfo.set(coordonnes);
    })
  }
  deletcoordonnees() {
    this.storage.get("user").then((user) => {

      let newInfo = firebase.database().ref(user.telephone + '/').remove();
    })
  }
  addInfo() {
    this.router.navigate(['/add-info']);
  }
  edit(key) {
    this.router.navigate(['/edit/' + key]);
  }

  async delete(key) {
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure want to delete this info?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('cancel');
          }
        }, {
          text: 'Okay',
          handler: () => {
            firebase.database().ref('infos/' + key).remove();
          }
        }
      ]
    });

    await alert.present();
  }
  async toast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async presentActionSheetDisabled(object) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      buttons: [
        {
          text: 'Recupérer(Paquet)',
          icon: 'checkbox-outline',
          handler: () => {
            //   this.recupererPaquet(object);
          }
        },
        {
          text: 'Cloturer',
          icon: 'pause',
          handler: () => {
            //this.cloturerCourse(object);
          }
        }, {
          text: 'Itineraire',
          icon: 'navigate',
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Appeler',
          icon: 'call',
          handler: () => {
            //this.appelleExpediteurOrDestinataire(object);
          }
        },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
    });
    await actionSheet.present();
  }

  async appelleExpediteurOrDestinataire() {
    this.storage.get("affectation").then(async aff => {
      console.log()
      const alert = await this.alertController.create({
        header: 'Appel',
        message: '<Strong><p>Veuillez choisir un numéro pour appeler</p></Strong>' +
          '<p>(1) Expediteur. ' + aff.telephoneExpediteur + '</p>' +
          '<p>(2) Destinataire. ' + aff.telephoneDestinataire + '</p>',
        inputs: [
          {
            name: 'name1',
            type: 'number',
            placeholder: ''
          }
        ],
        buttons: [
          {
            text: 'Annuler',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Appeler',
            handler: (data) => {
              if (data.name1 != "") {
                if (data.name1 == "1") {
                  this.appeller(aff.telephoneExpediteur);
                } else {
                  this.appeller(aff.telephoneDestinataire);
                }
                console.log(data);
              } else {
                console.log("erreur")
              }

            }
          }
        ]
      });

      await alert.present();
    })

  }
  async cloturerCourse() {
    this.storage.get("etat").then((paquet) => {
      if (paquet != null) {
        if (paquet == 2) {
          var message: string = "Je confirme avoir terminer la course!";
          this.confirmationMessageOptions(message, "terminer");
        } else {
          var message: string = "Vous ne pouvez pas cloturer la course actuellement";
          this.confirmationMessageOptions(message, "no");
        }
      } else {
        var message: string = "Vous ne pouvez pas cloturer la course actuellement";
        this.confirmationMessageOptions(message, "no");
      }
    })
  }
  async recupererPaquet() {

    var message: string = "Je confirme avoir recupérer le paquet!";

    this.confirmationMessageOptions(message, "recuperer");

  }
  async confirmationMessageOptions(message: string, etat: string) {
    const alert = await this.alertController.create({
      header: 'Info',
      message: '<strong><p>' + message + '</p></strong>',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // this.router.navigate(['affectation']);
          }
        }, {
          text: 'D\'accord',
          handler: () => {
            // this.progress = 0.5;
            this.storage.get("affectation").then
              (object => {
                console.log(object)
                console.log(etat)
                if (etat == "terminer") {
                  var terminer = { idDemande: object.idDemande, etat: 3, latitude: this.locationCoords.latitude, longitude: this.locationCoords.longitude }
                  console.log("doit etre stoper")
                 /*this.stopTracking();
                  this.deletcoordonnees();*/
                  // var value = { idDemande: object.idDemande, etat: "cloturer" }
                  this.servicesExpedition.updateAffectation(terminer).subscribe
                    (response => {
                      //Metre a jour la liste affichée
                      if (response) {
                        this.progress = 1;
                        this.storage.set("progresse", null);
                        console.log(response)

                        this.storage.set("etat", null);
                        this.storage.set("affectation", null)
                        this.storage.set("coursencours", null);
                        this.stopTracking();
                        this.deletcoordonnees();
                        this.showcloture = true;
                        var message: string = 'course cloturée.'
                        this.toast(message);
                      }


                    })

                } else if (etat == "recuperer") {
                  //Recuperer paquet
                 
                    var recuperer = { idDemande: object.idDemande, etat: 2 }
                    this.servicesExpedition.updateAffectation(recuperer).subscribe
                      (response => {
                        this.progress = 0.5;
                        this.storage.set("progresse", 0.5);
                        var val = {
                          idDemande: object.idDemande,
                          adresseExpedition: object.adresseExpedition,
                          adresseDestination: object.adresseDestination,
                          libelleExpedition: object.libelleExpedition,
                          libelleDestination: object.libelleDestination,
                          telephoneExpediteur: object.telephoneExpediteur,
                          telephoneDestinataire: object.telephoneExpediteur,
                          expediteur: object.expediteur,
                          destinataire: object.destinataire,
                          etatCourse: "Colis récupéré"
                        }
                        this.storage.set("affectation", val)
                        this.storage.set("etat", 2);
                        this.showrecuperer = true;
                        var message: string = 'Colis récupéré'
                        this.toast(message);
                        this.startTracking()
                      })
                
                  

                } else {

                }
              })
          }

        }
      ]
    });

    await alert.present();
  }
  loadGoogleMaps() {

    this.addConnectivityListeners();

    if (typeof google == "undefined" || typeof google.maps == "undefined") {

      console.log("Google maps JavaScript needs to be loaded.");
      this.disableMap();

      if (this.connectivityService.statConnection() == true) {
        console.log("online, loading map");
        this.apiKey = 'AIzaSyCjHJz6_vjoTtD6Ye-2YErz2FtJuXUnS-A';
        //Load the SDK
        window['mapInit'] = () => {
          this.initMap();
          this.enableMap();
        }

        let script = document.createElement("script");
        script.id = "googleMaps";

        if (this.apiKey) {
          script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
        } else {
          script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
        }

        document.body.appendChild(script);

      }
    }
    else {

      if (this.connectivityService.statConnection() == true) {
        console.log("showing map");
        this.initMap();
        this.enableMap();
      }
      else {
        console.log("disabling map");
        this.disableMap();
      }

    }

  }

  initMap() {
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.mapInitialised = true;
    console.log("Ma position actuellle")
    this.getPosition().then((resp) => {
      console.log(resp.coords.latitude + " " + resp.coords.longitude)

      this.storage.get("coursencours").then((position) => {
        console.log(position)
        var coordonneesDes = position.adresseDestination.split(',')
        console.log(coordonneesDes)
        var coordonneesDep = position.adresseExpedition.split(',')
        console.log(coordonneesDep)
        this.currentLat = coordonneesDep[0];
        this.currentLng = coordonneesDep[1];
        let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);

        let mapOptions = {
          center: latLng,
          zoom: 20,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.icon = "../../assets/img/marker.png";
        var infowindow = new google.maps.InfoWindow();

        var i;
        this.marker = new google.maps.Marker({
          position: new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude),
          map: this.map,
          icon: this.icon
        });

        google.maps.event.addListener(this.marker, 'click', (function (marker, i) {
          return function () {
            infowindow.setContent('Ma position actuelle');
            infowindow.open(this.map, marker);
          }
        })(this.marker, i));

        var latDepart: number = +coordonneesDep[0];
        var longDepart: number = +coordonneesDep[1];
        var latDes: number = +coordonneesDes[0];
        var longDes: number = +coordonneesDes[1];
        this.directionsDisplay.setMap(this.map);
        this.directionsService.route({
          origin: { lat: latDepart, lng: longDepart },
          //14.6929225,-17.4590014
          destination: { lat: latDes, lng: longDes },
          travelMode: 'DRIVING'
        }, (response, status) => {
          if (status === 'OK') {
            this.directionsDisplay.setDirections(response);
            this.startTracking();
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
        this.mappres=true;
        console.log("==============="+this.mappres);
      });



    }).catch((error) => {
      console.log('Error getting location', error);
    });


  }
  stopTracking() {
    console.log(this.trackedRoute)
    if (this.positionSubscription) {
      let newRoute = { finished: new Date().getTime(), path: this.trackedRoute };
      this.previousTracks.push(newRoute);
      this.storage.set('routes', this.previousTracks);

      this.isTracking = false;
      this.positionSubscription.unsubscribe();
      if(this.currentMapTrack){
        this.currentMapTrack.setMap(null);
      }else{
     /*   this.initMap();
        this.stopTracking();*/
      }
      
    }

  }
  markerPosition(lt: any, lg: any) {
    if (this.marker && this.marker.setMap) {
      this.marker.setMap(null);
    }
    let latLng = new google.maps.LatLng(lt, lg);
    this.map.setCenter(latLng);
    this.icon = "../../assets/img/marker.png";
    var infowindow = new google.maps.InfoWindow();

    var i;
    this.marker = new google.maps.Marker({
      position: new google.maps.LatLng(lt, lg),
      map: this.map,
      icon: this.icon
    });

    google.maps.event.addListener(this.marker, 'click', (function (marker, i) {
      return function () {
        infowindow.setContent('Ma position actuelle');
        infowindow.open(this.map, marker);
      }
    })(this.marker, i));

  }
  redrawPath(path) {
    this.icon = "../../assets/img/map-marker-red.png";
    if (this.currentMapTrack) {
      this.currentMapTrack.setMap(null);
    }

    if (path.length > 1) {
      this.currentMapTrack = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: '#ffffff',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        icon: this.icon
      });
      this.currentMapTrack.setMap(this.map);
    }
  }
  startTracking() {
    this.isTracking = true;
    this.trackedRoute = [];

    this.positionSubscription = this.geo.watchPosition()
      .pipe(filter((p) => p.coords !== undefined) //Filter Out Errors
      )
      .subscribe(data => {
        setTimeout(() => {
          console.log(data.coords.latitude + " " + data.coords.longitude)
          var coordonnees = {
            latitude: data.coords.latitude,
            longitude: data.coords.longitude
          }
          this.saveInfo(coordonnees);
          this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });
          this.storage.get("user").then(user => {
            this.storage.get("affectation").then
              (object => {
                var value = {
                  idLivreur: user.id,
                  idCourse: object.idDemande,
                  latitude: data.coords.latitude,
                  longitude: data.coords.longitude,
                  dateEnregistrement: new Date()
                }
                /*  this.servicesExpedition.envoyerTracking(value).subscribe(reponse=>{
                        console.log(reponse)
                  })*/
              });

          });

          this.markerPosition(data.coords.latitude, data.coords.longitude)
          this.redrawPath(this.trackedRoute);
        }, 0);
      });
  }
  disableMap() {
    console.log("disable map");
  }

  enableMap() {
    console.log("enable map");
  }

  addConnectivityListeners() {

    let onOnline = () => {

      setTimeout(() => {
        if (typeof google == "undefined" || typeof google.maps == "undefined") {

          this.loadGoogleMaps();

        } else {

          if (!this.mapInitialised) {
            this.initMap();
          }

          this.enableMap();
        }
      }, 2000);

    };

    let onOffline = () => {
      this.disableMap();
    };

    document.addEventListener('online', onOnline, false);
    document.addEventListener('offline', onOffline, false);

  }
  returnBack() {
    this.router.navigate(['courses']);
  }
  noteStart(start: string) {
    if (start == 'un') {
      if (this.un) {
        this.un = false;
      } else {
        this.un = true;
      }
    } else if (start == 'deux') {
      if (this.deux) {
        this.deux = false;
      } else {
        this.deux = true;
      }
    } else if (start == 'trois') {
      if (this.trois) {
        this.trois = false;
      } else {
        this.trois = true;
      }
    } else if (start == 'quatre') {
      if (this.quatre) {
        this.quatre = false;
      } else {
        this.quatre = true;
      }
    } else if (start == 'cinq') {
      if (this.cinq) {
        this.cinq = false;
      } else {
        this.cinq = true;
      }
    }

  }
  initvar() {
    this.storage.get("affectation").then(aff => {
      if (aff) {
        this.affectation = aff;
        if (aff.etatCourse == "Démarrée") {
          this.showrecuperer = false;
          this.showcloture = false;
        } else if (aff.etatCourse == "Colis récupéré") {
          this.showrecuperer = true;
          this.showcloture = false;

        } else {

        }
      }

    })

  }

  getPosition(): any {
    //**For Android**
    return this.geo.getCurrentPosition()
  }
  appeller(tel: string) {
    this.callNumber.callNumber(tel, true)
      .then(res => { })
      .catch(err => { });
  }
  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates()
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geo.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      this.locationCoords.timestamp = resp.timestamp;

    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }
}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};