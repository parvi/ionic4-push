import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ConnectivityService } from '../connectivity.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Subscription } from 'rxjs';
import { Storage } from '@ionic/storage';
import { filter } from 'rxjs/operators';
import { GoogleMaps, GoogleMap, CameraPosition, LatLng, GoogleMapsEvent } from '@ionic-native/google-maps';


declare var google;
@Component({
  selector: 'app-test',
  templateUrl: './test.page.html',
  styleUrls: ['./test.page.scss'],
})
export class TestPage  {

  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('directionsPanel') directionsPanel: ElementRef;
  map: any;
  mapInitialised: boolean = false;
  apiKey: any;
  mapshow: boolean = false;
  marker: any
  icon: string;
  nom: string = '';
  directionsService: any;
  directionsDisplay: any;
  currentLat: any = null;
  currentLng: any = null;
  trackedRoute: any;
  currentMapTrack = null;
  positionSubscription: Subscription;
  constructor(public navCtrl: NavController,
    public connectivityService: ConnectivityService,
    private geo: Geolocation,
    private storage: Storage) {
      this.loadMap();
     // this.startNavigating();
  }

  /*ionViewDidLoad(){

      this.loadMap();
      this.startNavigating();

  }
*/
  loadMap(){

      this.addConnectivityListeners();

      if (typeof google == "undefined" || typeof google.maps == "undefined") {
  
        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();
  
        if (this.connectivityService.statConnection() == true) {
          console.log("online, loading map");
          this.apiKey = 'AIzaSyCjHJz6_vjoTtD6Ye-2YErz2FtJuXUnS-A';
          //Load the SDK
          window['mapInit'] = () => {
            this.initMap();
            this.startNavigating();
            this.enableMap();
          }
  
          let script = document.createElement("script");
          script.id = "googleMaps";
  
          if (this.apiKey) {
            script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
          } else {
            script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
          }
  
          document.body.appendChild(script);
  
        }
      }
      else {
  
        if (this.connectivityService.statConnection() == true) {
          console.log("showing map");
          this.initMap();
          this.startNavigating();
          this.enableMap();
        }
        else {
          console.log("disabling map");
          this.disableMap();
        }
  
      }
  

  }
  addConnectivityListeners() {

    let onOnline = () => {

      setTimeout(() => {
        if (typeof google == "undefined" || typeof google.maps == "undefined") {

          this.loadMap();

        } else {

          if (!this.mapInitialised) {
            this.initMap();
            this.startNavigating();
          }

          this.enableMap();
        }
      }, 2000);

    };

    let onOffline = () => {
      this.disableMap();
    };

    document.addEventListener('online', onOnline, false);
    document.addEventListener('offline', onOffline, false);

  }
  disableMap() {
    console.log("disable map");
  }

  enableMap() {
    console.log("enable map");
  }

  initMap() {
    /*
      let latLng = new google.maps.LatLng(-34.9290, 138.6010);

      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);*/
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    this.mapInitialised = true;
    console.log("Ma position actuellle")
    this.getPosition().then((resp) => {
      console.log(resp.coords.latitude + " " + resp.coords.longitude)

     //   console.log(position)
      /*  var coordonneesDes = position.adresseDestination.split(',')
        console.log(coordonneesDes)
        var coordonneesDep = position.adresseExpedition.split(',')
        console.log(coordonneesDep)*/
      /*  this.currentLat = coordonneesDep[0];
        this.currentLng = coordonneesDep[1];*/
        let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
        //let latLng = new google.maps.LatLng(-34.9290, 138.6010);

        let mapOptions = {
          center: latLng,
          zoom: 20,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        this.icon = "../../assets/img/marker.png";
        var infowindow = new google.maps.InfoWindow();

        var i;
        this.marker = new google.maps.Marker({
          position: new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude),
          map: this.map,
          icon: this.icon
        });

        google.maps.event.addListener(this.marker, 'click', (function (marker, i) {
          return function () {
            infowindow.setContent('Ma position actuelle');
            infowindow.open(this.map, marker);
          }
        })(this.marker, i));

    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }
  getPosition(): any {
    //**For Android**
    return this.geo.getCurrentPosition()
  }
  startNavigating(){
   
    this.getPosition().then((resp) => {
      
      console.log(resp.coords.latitude + "=== " + resp.coords.longitude)
      let directionsService = new google.maps.DirectionsService;
     // let directionsDisplay = new google.maps.DirectionsRenderer();
     
      this.directionsDisplay.setMap(this.map);
      this.directionsDisplay.setPanel(this.directionsPanel.nativeElement);
     // 14.6929225,-17.4590014
      directionsService.route({
          //origin: 'adelaide',
          origin:  { lat: resp.coords.latitude, lng: resp.coords.longitude },
       //   14.7882749,-17.3131905
          destination: { lat: 14.7882749, lng: -17.3131905 },
          travelMode: google.maps.TravelMode['DRIVING']
         
      }, (res, status) => {

          if(status == google.maps.DirectionsStatus.OK){
              //directionsDisplay = new google.maps.DirectionsRenderer();
             this.directionsDisplay.setDirections(res);
              this.startTracking();
          } else {
              console.warn(status);
          }

      });
    })
     

  }
  markerPosition(lt: any, lg: any) {
    if (this.marker && this.marker.setMap) {
      this.marker.setMap(null);
    }
    let latLng = new google.maps.LatLng(lt, lg);
    this.map.setCenter(latLng);
    this.icon = "../../assets/img/marker.png";
    var infowindow = new google.maps.InfoWindow();

    var i;
    this.marker = new google.maps.Marker({
      position: new google.maps.LatLng(lt, lg),
      map: this.map,
      icon: this.icon
    });

    google.maps.event.addListener(this.marker, 'click', (function (marker, i) {
      return function () {
        infowindow.setContent('Ma position actuelle');
        infowindow.open(this.map, marker);
      }
    })(this.marker, i));

  }
  redrawPath(path) {
    this.icon = "../../assets/img/map-marker-red.png";
    if (this.currentMapTrack) {
      this.currentMapTrack.setMap(null);
    }

    if (path.length > 1) {
      this.currentMapTrack = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: '#ffffff',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        icon: this.icon
      });
      this.currentMapTrack.setMap(this.map);
    }
  }
 

  startTracking() {
  //  this.isTracking = true;
    this.trackedRoute = [];

    this.positionSubscription = this.geo.watchPosition()
      .pipe(filter((p) => p.coords !== undefined) //Filter Out Errors
      )
      .subscribe(data => {
        setTimeout(() => {
          console.log(data.coords.latitude + " " + data.coords.longitude)
          var coordonnees = {
            latitude: data.coords.latitude,
            longitude: data.coords.longitude
          }
       //   this.saveInfo(coordonnees);
          this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });
      
          this.markerPosition(data.coords.latitude, data.coords.longitude)
          this.redrawPath(this.trackedRoute);
          this.startNavigating();
        }, 0);
      });
  }

  

}
