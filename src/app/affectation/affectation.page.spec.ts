import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectationPage } from './affectation.page';

describe('AffectationPage', () => {
  let component: AffectationPage;
  let fixture: ComponentFixture<AffectationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
