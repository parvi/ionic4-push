import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ToastController, LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ExpeditionProvider } from '../api/expedition.service';
import * as firebase from 'Firebase';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { filter } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-affectation',
  templateUrl: './affectation.page.html',
  styleUrls: ['./affectation.page.scss'],
})
export class AffectationPage implements OnInit {
  encours: boolean = false
  listExpedition: any;
  locationCoords: any;
  timetest: any;
  isTracking: boolean = false;
  trackedRoute: any;
  positionSubscription: Subscription;
  showcourseencours: boolean = false;
  affectationencours: any = null;
  showlist: boolean = false;
  user = null;
  constructor(public actionSheetController: ActionSheetController,
    public alertController: AlertController,
    private storage: Storage,
    private router: Router,
    private toastController: ToastController,
    private servicesExpedition: ExpeditionProvider,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    private geolocation: Geolocation,
    private loadingController: LoadingController,
    private afAuth: AngularFireAuth,
  ) {
    this.storage.get("etat").then((val) => {
      if (val != null) {
        if (val) {

          this.encours = true;
        }
      }
    })
    this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: ""
    }
    this.checkGPSPermission();
    //this.storage.set("affectation",null);
    //this.getLocationCoordinates();
    this.mesaffectation();
  }
  ngOnInit() {
  }
  index(item): number {
    var index = this.listExpedition.indexOf(item) + 1;
    return index;
  }
  async mesaffectation() {
    /*this.storage.set("affectation", null);
    this.storage.set("coursencours", null);
    this.storage.set("etat", null)*/
    this.coursesEncours();
    var value = {
      "idLivreur": 18
    }
    this.storage.get("user").then(async user => {
      console.log(user)
      var livreur = {
        "idLivreur": user.id
      }
      this.servicesExpedition.mesaffectionExpedition(livreur).subscribe(list => {
        console.log(list)

        this.listExpedition = [];
        if (list) {
          this.showlist = false;
          this.storage.get("affectation").then(aff => {
            if (aff != null) {
              for (let e of list) {
                if (e.idDemande == aff.idDemande) {
                  let index = list.indexOf(e);
                  if (index > -1) {

                    list[index] = aff;

                    this.encours = true;
                    var message: string = 'Vous deja une course en cours.'
                    this.toast(message);
                  }

                }
              }
            }
          })
          this.listExpedition = list;
        } else {
          this.showlist = false;
        }

        // console.log(list)
      }, err => {
        console.log(err)
      })
    })
  }
  ionViewWillEnter() {
    this.coursesEncours();
    this.mesaffectation();
  }
  coursesEncours() {
    this.storage.get("coursencours").then(aff => {
      if (aff) {
        this.showcourseencours = true;
        this.affectationencours = aff;
      } else {
        this.showcourseencours = false;
        this.affectationencours = null;
      }
    })
  }

  returnBack() {
    this.router.navigate(['home']);
  }
  async demarerCourse(object: any) {
    var message: string = 'Je confirme le démarage de la course sous ma responsabilité';
    this.storage.set("affectation", object);
    this.confirmationMessageOptions(message, "en attente");
  }
  async toast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  async presentActionSheet(object: any) {
    /* this.storage.get("etat").then((val) => {
       if (val != null) {
         if (val) {
           this.presentActionSheetDisabled(object);
         }
       } else {
         this.presentActionSheetNull(object);
       }
     })*/
    //================================================
    this.storage.get("coursencours").then(aff => {
      if (aff) {
        if (aff.idDemande == object.idDemande) {
          this.presentActionSheetEnabled();
        } else {
          var message = "Une course est dèjà démarée";
          this.toast(message);
        }
        // this.showcourseencours = true;
        // this.affectationencours = aff;
      } else {
        this.presentActionSheetNull(object);
      }
    })

  }
  async presentActionSheetother(object: any) {
    this.storage.get("etat").then((val) => {
      if (val != null) {
        if (val) {
          this.presentActionSheetEnabled();
        }
      } else {
        this.presentActionSheetNull(object);
      }
    })

    //============================================


  }
  async presentActionSheetNull(object: any) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      buttons: [{
        text: 'Démarer',
        icon: 'play',
        handler: () => {
          this.demarerCourse(object)
        }
      },
      {
        text: 'Annuler',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  async presentActionSheetDisabled(object) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      buttons: [

        {
          text: 'Suivre la course',
          icon: 'eye',
          handler: () => {
            this.detailsCourseEnCours();
          }
        }, {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
    });
    await actionSheet.present();
  }

  async presentActionSheetEnabled() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      buttons: [
        {
          text: 'Suivre la course',
          icon: 'more',
          handler: () => {
            this.detailsCourseEnCours();
          }
        }, {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
    });
    await actionSheet.present();
  }
  expeditionPage() {
    this.storage.get("etat").then((val) => {
      if (val != null) {
        if (val) {
          this.presentAlertConfirm0();
        }
      } else {
        this.router.navigate(['affectation']);
      }
    })

    // 
  }

  async presentAlertConfirm0() {
    const alert = await this.alertController.create({
      header: 'Confirmation',

      message: '<strong "><p>Vous avez actuellement une course en cours</p></strong>',
      buttons: [
        {
          text: 'Poursuivre',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['affectation']);
          }
        }, {
          text: 'Voire la liste',
          handler: () => {
            this.router.navigate(['affectation']);
          }
        }
      ]
    });

    await alert.present();
  }
  async appelleExpediteurOrDestinataire(object: any) {

    const alert = await this.alertController.create({
      header: 'Appel',
      message: '<Strong><p>Veuillez choisir un numéro pour appeler</p></Strong>' +
        '<p>(1) Expediteur. ' + object.telephoneExpediteur + '</p>' +
        '<p>(2) Destinataire. ' + object.telephoneDestinataire + '</p>',
      inputs: [
        {
          name: 'name1',
          type: 'number',
          placeholder: ''
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Appeler',
          handler: (data) => {
            if (data.name1 != "") {
              console.log(data);
            } else {
              console.log("erreur")
            }

          }
        }
      ]
    });

    await alert.present();
  }
  async cloturerCourse(object: any) {
    this.storage.get("paquet").then((paquet) => {
      if (paquet != null) {
        if (paquet) {
          var message: string = "Je confirme avoir terminer la course!";
          this.confirmationMessageOptions(message, "terminer");

        }
      } else {
        var message: string = "Vous ne pouvez pas cloturer la course actuellement";
        this.confirmationMessageOptions(message, "no");
      }
    })
  }
  async recupererPaquet(object: any) {
    var message: string = "Je confirme avoir recupérer le paquet!";
    this.storage.set("affectation", object);
    this.confirmationMessageOptions(message, "recuperer");

  }
  async confirmationMessageOptions(message: string, etat: string) {
    const alert = await this.alertController.create({
      header: 'Info',
      message: '<strong><p>' + message + '</p></strong>',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // this.router.navigate(['affectation']);
          }
        }, {
          text: 'D\'accord',
          handler: () => {
            this.storage.get("affectation").then
              (object => {

                if (etat == "terminer") {
                  var terminer = { idDemande: object.idDemande, etat: "terminer" }
                  // var value = { idDemande: object.idDemande, etat: "cloturer" }
                  this.servicesExpedition.updateAffectation(terminer).subscribe
                    (response => {
                      //Metre a jour la liste affichée
                      let index = this.listExpedition.indexOf(object)
                      if (index > -1) {
                        var val = {
                          idDemande: object.idDemande,
                          adresseExpedition: object.adresseExpedition,
                          adresseDestination: object.adresseDestination,
                          libelleExpedition: object.libelleExpedition,
                          libelleDestination: object.libelleDestination,
                          telephoneExpediteur: object.telephoneExpediteur,
                          telephoneDestinataire: object.telephoneExpediteur,
                          expediteur: object.expediteur,
                          destinataire: object.destinataire,
                          etat: "terminer"
                        }
                        this.listExpedition[index] = val;
                      }

                      this.storage.set("etat", null);
                      this.storage.set("paquet", null);
                      this.encours = false;
                      var message: string = 'course cloturée.'
                      this.toast(message);
                    })

                } else if (etat == "en attente") {
                  if (object.etatCourse == "En attente") {
                    this.storage.get("coursencours").then(aff => {
                      console.log(aff)
                      if (aff) {
                        var message: string = 'Une course est deja en cours.'
                        this.toast(message);
                      } else {
                        //Demarer la course
                        console.log("on met a jour")
                        var demarer = { "idDemande": object.idDemande, "etat": 1, "latitude": this.locationCoords.latitude, "longitude": this.locationCoords.longitude }
                        console.log(demarer);
                        this.servicesExpedition.updateAffectation(demarer).subscribe
                          (response => {
                            console.log(response)
                            var val: any;
                            val = {
                              idDemande: object.idDemande,
                              adresseExpedition: object.adresseExpedition,
                              adresseDestination: object.adresseDestination,
                              libelleExpedition: object.libelleExpedition,
                              libelleDestination: object.libelleDestination,
                              telephoneExpediteur: object.telephoneExpediteur,
                              telephoneDestinataire: object.telephoneExpediteur,
                              expediteur: object.expediteur,
                              destinataire: object.destinataire,
                              etatCourse: "Démarrée"
                            }
                            this.storage.set("affectation", val);
                            // this.storage.set("coursencours", val);
                            this.storage.set("etat", 1);
                            this.encours = true;
                            this.showcourseencours = true;
                            //    var message: string = 'course démarée.'
                            //    this.startTracking();
                            //   this.toast(message);
                            this.alertDemarage();
                          }, err => {
                            console.log(err)
                          })
                      }
                    })


                  } else {
                    var message: string = 'course deja démarée.'

                    this.toast(message);
                  }

                } else if (etat == "recuperer") {
                  //Recuperer paquet
                  var recuperer = { idDemande: object.idDemande, etat: "recuperer" }
                  this.servicesExpedition.updateAffectation(recuperer).subscribe
                    (response => {
                      //Metre a jour la liste affichée
                      let index = this.listExpedition.indexOf(object)
                      if (index > -1) {
                        var val = {
                          idDemande: object.idDemande,
                          adresseExpedition: object.adresseExpedition,
                          adresseDestination: object.adresseDestination,
                          libelleExpedition: object.libelleExpedition,
                          libelleDestination: object.libelleDestination,
                          telephoneExpediteur: object.telephoneExpediteur,
                          telephoneDestinataire: object.telephoneExpediteur,
                          expediteur: object.expediteur,
                          destinataire: object.destinataire,
                          etat: "demarer"
                        }
                        this.listExpedition[index] = val;
                      }
                      this.storage.set("paquet", true);
                      var message: string = 'Pacquet recupéré.'
                      this.toast(message);
                    })

                } else {

                }
              })
          }

        }
      ]
    });

    await alert.present();
  }
  async alertDemarage() {
    const alert = await this.alertController.create({
      header: 'Information',
      message: '<Strong><p>Vous avez démaré une nouvelle course</p></Strong>',
      buttons: [
        {
          text: 'D\'accords',
          handler: (data) => {
            //enregistrer sont device device dans le firebase
            this.anonLogin();


          }
        }
      ]
    });

    await alert.present();

  }
  async detailsCourseEnCours() {
    //console.log(object)
    //this.storage.set("coursencours", null);
    //  this.storage.set("etat", null)
    this.router.navigate(['coursedemarer']);
  }

  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates()
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }
  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      this.locationCoords.timestamp = resp.timestamp;

    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }
  saveInfo(coordonnes: any) {
    this.storage.get("user").then((user) => {
      console.log(user)
      let newInfo = firebase.database().ref(user.telephone + '/').push();
      newInfo.set(coordonnes);
    })
  }
  // Perform an anonymous login and load data
  anonLogin() {
    this.afAuth.auth.signInAnonymously().then(res => {
      this.user = res.user;
      console.log(this.user.uid);
      this.storage.get("user").then((user) => {
        console.log(user)
        let newInfoo = firebase.database().ref(user.telephoneLivreur + '/').on('value', resp => {
          var coordonnes = [];
          coordonnes = snapshotToArray(resp);
          if (coordonnes.length > 0 || coordonnes != null) {
            var object = { iddevice: this.user.uid }
            if (coordonnes.length > 1) {
              var key = coordonnes[0].key;
              let updateObject = firebase.database().ref(user.telephone + '/' + key).update(object);
              this.router.navigate(['homme']);
            } else {
              let newInfo = firebase.database().ref(user.telephone + '/').push();
              newInfo.set(object);
              this.router.navigate(['homme']);
            }
          }

        });

      })
    });
  }


  startTracking() {
    this.isTracking = true;
    this.trackedRoute = [];

    this.positionSubscription = this.geolocation.watchPosition()
      .pipe(filter((p) => p.coords !== undefined) //Filter Out Errors
      )
      .subscribe(data => {
        setTimeout(() => {
          console.log(data.coords.latitude + " " + data.coords.longitude)
          var coordonnees = {
            latitude: data.coords.latitude,
            longitude: data.coords.longitude
          }
          // this.saveInfo(coordonnees);
          this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });

        }, 0);
      });
  }

}
export const snapshotToArray = snapshot => {
  let returnArr = [];

  snapshot.forEach(childSnapshot => {
    let item = childSnapshot.val();
    item.key = childSnapshot.key;
    returnArr.push(item);
  });

  return returnArr;
};
