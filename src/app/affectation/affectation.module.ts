import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AffectationPage } from './affectation.page';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { MainservicesService } from '../mainservices.service';
import { UserService } from '../api/user.service';
import { ExpeditionProvider } from '../api/expedition.service';

const routes: Routes = [
  {
    path: '',
    component: AffectationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AffectationPage],
  providers: [
    AndroidPermissions,
    Geolocation,
    LocationAccuracy,
    MainservicesService,
    UserService,
    ExpeditionProvider,
  ],
})
export class AffectationPageModule {}
