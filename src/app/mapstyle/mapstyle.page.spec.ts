import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapstylePage } from './mapstyle.page';

describe('MapstylePage', () => {
  let component: MapstylePage;
  let fixture: ComponentFixture<MapstylePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapstylePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapstylePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
