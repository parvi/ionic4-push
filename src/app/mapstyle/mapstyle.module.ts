import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { AgmCoreModule } from '@agm/core';
import { Routes, RouterModule } from '@angular/router';


import { MapstylePage } from './mapstyle.page';

const routes: Routes = [
  {
    path: '',
    component: MapstylePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCjHJz6_vjoTtD6Ye-2YErz2FtJuXUnS-A'
    })
  ],
  declarations: [MapstylePage]
})
export class MapstylePageModule {}
