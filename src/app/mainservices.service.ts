import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MainservicesService {
  public apiUrl = "http://dht.htsoftdemo.com/api";
  public apiUrllocal ="http://localhost/wsdht/";
  constructor() { }
}
