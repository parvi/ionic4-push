import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailcoursepagePage } from './detailcoursepage.page';

describe('DetailcoursepagePage', () => {
  let component: DetailcoursepagePage;
  let fixture: ComponentFixture<DetailcoursepagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailcoursepagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailcoursepagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
