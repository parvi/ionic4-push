import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { LoadingController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ExpeditionProvider } from '../api/expedition.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
@Component({
  selector: 'app-detailcoursepage',
  templateUrl: './detailcoursepage.page.html',
  styleUrls: ['./detailcoursepage.page.scss'],
})
export class DetailcoursepagePage implements OnInit {
  commerce: string = ''
  timeBegan: any = null
  timeStopped: any = null
  stoppedDuration: any = 0
  started: any = null
  running: boolean = false
  blankTime: string = "00:00.000"
  time: string = "00:00.000"
  dateDebut: any
  etatAffectation: string=""
  heureDebut: string
  heureFin: string
  idAffectationcommerce: any
  idTypetarif: any
  montant: any
  positionCommerce: any;
  latitude: number;
  longitude: number;
  locationCoords: any;
  nbExpedition: number;
  terminer: boolean;
  constructor(private storage: Storage,
    private loadingController: LoadingController,
    private route: Router,
    private serviceExpedition: ExpeditionProvider,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    private alertController: AlertController) {
    // this.commerce = 'Graine d\'Or'
    this.nombreExpedition();
    this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: ""
    }
    //this.checkGPSPermission();
    this.getLocationCoordinates();
  }
  ionViewWillEnter(){
    this.nombreExpedition();
  }
  ngOnInit() {
    this.storage.get("coursespeciale").then(course => {
      
      if (course) {
        if (course.etatAffectation == "En attente") {
          this.terminer = false;
        } else {
          this.terminer = true;
        }
        this.etatAffectation = course.etatAffectation
        this.commerce = course.commerce
        this.dateDebut = course.dateDebut
        this.heureDebut = course.heureDebut
        this.heureFin = course.heureFin
        this.idAffectationcommerce = course.idAffectationcommerce
        this.idTypetarif = course.idTypetarif
        this.montant = course.montant
        this.positionCommerce = course.positionCommerce

      }

    })
  }
  nombreExpedition() {
    this.storage.get("nbExpCommerce").then(nb => {
      if (nb) {
        this.nbExpedition = nb;
      } else {
        this.nbExpedition = 0;
      }
    })
  }
  start() {
    if (this.running) return;
    if (this.timeBegan === null) {
      this.reset();
      this.timeBegan = new Date();
    }
    if (this.timeStopped !== null) {
      let newStoppedDuration: any = (+new Date() - this.timeStopped)
      this.stoppedDuration = this.stoppedDuration + newStoppedDuration;
    }
    this.started = setInterval(this.clockRunning.bind(this), 10);
    this.running = true;

  }
  stop() {
    this.running = false;
    this.timeStopped = new Date();
    clearInterval(this.started);
  }
  reset() {
    this.running = false;
    clearInterval(this.started);
    this.stoppedDuration = 0;
    this.timeBegan = null;
    this.timeStopped = null;
    this.time = this.blankTime;
  }
  zeroPrefix(num, digit) {
    let zero = '';
    for (let i = 0; i < digit; i++) {
      zero += '0';
    }
    return (zero + num).slice(-digit);
  }
  clockRunning() {
    let currentTime: any = new Date()
    let timeElapsed: any = new Date(currentTime - this.timeBegan - this.stoppedDuration)
    let hour = timeElapsed.getUTCHours()
    let min = timeElapsed.getUTCMinutes()
    let sec = timeElapsed.getUTCSeconds()
    let ms = timeElapsed.getUTCMilliseconds();
    this.time =
      this.zeroPrefix(hour, 2) + ":" +
      this.zeroPrefix(min, 2) + ":" +
      this.zeroPrefix(sec, 2) + "." +
      this.zeroPrefix(ms, 3);
  };

  nouvellecourse() {
    this.route.navigate(['expedition']);
  }
  async demarerService() {
    this.start();
    this.terminer = true;
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      message: '...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    this.storage.get("coursespeciale").then(course => {
      this.storage.get("user").then(user => {
        console.log(course)
        console.log(user)
        course.etatAffectation = "Demarage"
        this.etatAffectation = "Demarage";
        console.log(course);
        this.storage.set("coursespeciale", course);
        loading.dismiss();
        //var etat = 2;
        var value = {
          idLivreur: user.id,
          idAffectationcommerce: course.idAffectationcommerce,
          idEtataffectation: 2,
          latitude: this.locationCoords.latitude,
          longitude: this.locationCoords.longitude
        }
        console.log(value)
        this.serviceExpedition.optionAffectation(value).subscribe(reponse => {
          console.log(reponse);
          this.storage.set("courseSpecialDemarer",course);
        })
      })

    })
  }
  async stoperService() {
    this.stop();
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      message: '...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    this.storage.get("coursespeciale").then(course => {

      this.storage.get("user").then(user => {
        
        this.storage.set("nbExpCommerce", null);

        var value = {
          idLivreur: user.id,
          idAffectationcommerce: course.idAffectationcommerce,
          idEtataffectation: 3,
          latitude: this.locationCoords.latitude,
          longitude: this.locationCoords.longitude
        }
        console.log(value)
        
        
        this.serviceExpedition.optionAffectation(value).subscribe(reponse => {
          loading.dismiss();
          console.log(reponse);

          var value = {
            idAffectationcommerce: course.idAffectationcommerce
          }

          this.serviceExpedition.monterAverser(value).subscribe(montant => {
            console.log(montant)
            this.confirmationMessageOptions('Le montant à deposer est à : ' + montant.reponse);
          }, error => {
            loading.dismiss();
          })


        }, err => {
          loading.dismiss();
        })

      },err=>{
        loading.dismiss();
      })



    },err=>{
      loading.dismiss();
    })
    
  }
  async confirmationMessageOptions(message: string) {
    
    
    const alert = await this.alertController.create({
      header: 'Alert',
      message: '<strong><p>' + message + '</p></strong>',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            this.storage.set("coursespeciale", null);
            this.storage.set("courseSpecialDemarer", null);
            this.route.navigate(['coursespeciale']);
          }
        }
      ]
    });

    await alert.present();
  }
  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {

          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {

          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }

  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }

  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates()
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }

  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      this.locationCoords.timestamp = resp.timestamp;

    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }
}
