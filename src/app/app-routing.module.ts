import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
 // { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'second/:price', loadChildren: './second/second.module#SecondPageModule' },
  { path: 'connexion', loadChildren: './connexion/connexion.module#ConnexionPageModule' },
  { path: 'expedition', loadChildren: './expedition/expedition.module#ExpeditionPageModule' },
  { path: 'affectation', loadChildren: './affectation/affectation.module#AffectationPageModule' },
  { path: 'coursedemarer', loadChildren: './coursedemarer/coursedemarer.module#CoursedemarerPageModule' },
  { path: 'compte', loadChildren: './compte/compte.module#ComptePageModule' },
  { path: 'mescourses', loadChildren: './mescourses/mescourses.module#MescoursesPageModule' },
  { path: 'create', loadChildren: './create/create.module#CreatePageModule' },
  { path: 'detail', loadChildren: './detail/detail.module#DetailPageModule' },
  { path: 'edit', loadChildren: './edit/edit.module#EditPageModule' },
  { path: 'coursespeciale', loadChildren: './coursespeciale/coursespeciale.module#CoursespecialePageModule' },
  { path: 'detailcoursepage', loadChildren: './detailcoursepage/detailcoursepage.module#DetailcoursepagePageModule' },
  { path: 'expeditioncommerciale', loadChildren: './expeditioncommerciale/expeditioncommerciale.module#ExpeditioncommercialePageModule' },
  { path: 'test', loadChildren: './test/test.module#TestPageModule' },
  { path: 'homme', loadChildren: './homme/homme.module#HommePageModule' },
  { path: 'mapstyle', loadChildren: './mapstyle/mapstyle.module#MapstylePageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
