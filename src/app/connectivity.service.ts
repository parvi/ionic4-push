import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';


declare var Connection;
declare var AdvancedGeolocation:any;
@Injectable({
  providedIn: 'root'
})

export class ConnectivityService {
  onDevice: boolean;
  currentLat:any;
  currentLng:any;
  constructor(public platform: Platform, private network: Network,
    private geolocation: Geolocation) {
    this.onDevice = this.platform.is('cordova');
    this.statConnection();
  }


  public statConnection(){
    if(this.isOnline()) {
      // Code required to initialize the page if the user is online
      return true;
    } else {
      return false;
      // What to do when the user is offline
    }
  }
  private isOnline(): boolean {
    return this.network.type !== 'none';
  }
  getPosition() :any{
    //**For Android**
    var positionCordonnee:any;
    if (this.platform.is('android')) {
      this.platform.ready().then(() => {
        AdvancedGeolocation.start((success) => {
          //loading.dismiss();
          // this.refreshCurrentUserLocation();
          try {
            var jsonObject = JSON.parse(success);
            console.log("Provider " + JSON.stringify(jsonObject));
            switch (jsonObject.provider) {
              case "gps":
                console.log("setting gps ====<<>>" + jsonObject.latitude);
                positionCordonnee = {latitude:jsonObject.latitude,longitude:jsonObject.longitude}
                this.currentLat = jsonObject.latitude;
                this.currentLng = jsonObject.longitude;
                break;
  
              case "network":
                console.log("setting network ====<<>>" + jsonObject.latitude);
                positionCordonnee = {latitude:jsonObject.latitude,longitude:jsonObject.longitude}
               
                this.currentLat = jsonObject.latitude;
                this.currentLng = jsonObject.longitude;
  
                break;
  
              case "satellite":
                //TODO
                break;
  
              case "cell_info":
                //TODO
                break;
  
              case "cell_location":
                //TODO
                break;
  
              case "signal_strength":
                //TODO
                break;
            }
          }
          catch (exc) {
            console.log("Invalid JSON: " + exc);
          }
        },
          function (error) {
            console.log("ERROR! " + JSON.stringify(error));
          },
          {
            "minTime": 500,         // Min time interval between updates (ms)
            "minDistance": 1,       // Min distance between updates (meters)
            "noWarn": true,         // Native location provider warnings
            "providers": "all",     // Return GPS, NETWORK and CELL locations
            "useCache": true,       // Return GPS and NETWORK cached locations
            "satelliteData": false, // Return of GPS satellite info
            "buffer": false,        // Buffer location data
            "bufferSize": 0,         // Max elements in buffer
            "signalStrength": false // Return cell signal strength data
          });
  
      });
      return positionCordonnee ;
    } else if(this.platform.is('android')){
  
      // **For IOS**
  /*
      let options = {
        frequency: 1000,
        enableHighAccuracy: false
      };
  
      this.watch = this.geolocation.watchPosition(options).filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {
        // loading.dismiss();
        console.log("current location at login" + JSON.stringify(position));
  
        // Run update inside of Angular's zone
        this.zone.run(() => {
          this.currentLat = position.coords.latitude;
          this.currentLng = position.coords.longitude;
        });
  
      });
    }*/
  }else{
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
     }).catch((error) => {
       console.log('Error getting location', error);
     });
     
     let watch = this.geolocation.watchPosition();
     watch.subscribe((data) => {
      positionCordonnee = {latitude:data.coords.latitude,longitude:data.coords.longitude}
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
     });
     return positionCordonnee;
  }
  }
}
