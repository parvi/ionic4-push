import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { UserService } from '../api/user.service';
import { async } from 'q';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {
  telephone: any = "";
  pwd: any = "";
  erroridentifiant: boolean = false;
  emptyform: boolean = false;
  
  constructor(private storage: Storage,
    private router: Router,
    private servicesUser: UserService,
    public loadingController: LoadingController,
    public alertController: AlertController, ) { }

  ngOnInit() {
  }
  async connecter() {
    if (this.telephone != "" && this.pwd != "") {
      var user = {
        "telephone": this.telephone,
        "motDePasse": this.pwd
      };
      console.log(user)
      const loading = await this.loadingController.create({
        spinner: "crescent",
        message: 'Connexion en cours...',
        translucent: true,
        cssClass: 'custom-class custom-loading'
      });
      loading.present();
      this.servicesUser.connexion(user).subscribe(value => {
        if (value) {
          loading.dismiss();
          if (value.isLogged == 0) {
            var message = "Numéro ou mot de passe incorrect !"
            this.confirmationMessageOptions(message);
          } else if(value.isLogged == 1){
            this.storage.get("tokenFirebase").then(token=>{
              var obj={
                idUtilisateur:value.id,
                token:token
              }
              this.storage.set("user", value);
              this.router.navigate(['home']);
              loading.dismiss(); 
             /* this.servicesUser.sendTokenFireBase(obj).subscribe(res=>{
                      if(obj){
                        this.storage.set("user", value);
                        this.router.navigate(['home']);
                        loading.dismiss(); 
                      }
              })*/
            },error=>{
              loading.dismiss()
            })
            
          }
          console.log(value);
        }
      }, (error) => {
        loading.dismiss();
        console.log(error);
      })
      // this.storage.set("user", user);

      //  this.router.navigate(['home']);
    } else {
      this.emptyform = true
    }
  }
  async confirmationMessageOptions(message: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: '<strong><p>' + message + '</p></strong>',
      buttons: [
        {
          text: 'OK',
          handler: () => {

          }
        }
      ]
    });

    await alert.present();
  }
  hidemessage() {
    this.emptyform = false;
  }
}
