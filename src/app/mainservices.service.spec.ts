import { TestBed } from '@angular/core/testing';

import { MainservicesService } from './mainservices.service';

describe('MainservicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MainservicesService = TestBed.get(MainservicesService);
    expect(service).toBeTruthy();
  });
});
