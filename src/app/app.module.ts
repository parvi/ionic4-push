
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
//import { FCM } from '@ionic-native/fcm/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule, HttpClientJsonpModule  } from '@angular/common/http';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//import { FCM } from '@ionic-native/fcm/ngx';
/*import { SignalRModule } from 'ng2-signalr';
import { SignalRConfiguration } from 'ng2-signalr';*/
/*export function createConfig(): SignalRConfiguration {
  let config = new SignalRConfiguration();
  config.url = 'http://localhost:8000/signalr/';
  config.hubName = "diyhub";
  config.logging = true;
  config.executeErrorsInZone = true;
  config.executeEventsInZone = true;
  config.executeStatusChangeInZone = true;
  return config;
}*/

 
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
const   firebase = {
  apiKey: "AIzaSyBAA6qZUTTFWFDYoO-KrH2NHpbj-qNV42g",
  authDomain: "dht-htsoft.firebaseapp.com",
  databaseURL: "https://dht-htsoft.firebaseio.com",
  projectId: "dht-htsoft",
  storageBucket: "gs://dht-htsoft.appspot.com/",
  messagingSenderId: "133539155181"
}
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [ 
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    IonicModule.forRoot(), 
    HttpClientModule,
    HttpClientJsonpModule,
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(firebase),
    AngularFirestoreModule,
    AngularFireAuthModule
  //  SignalRModule.forRoot(createConfig),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    //MainservicesService,
    //UserService,
    //ExpeditionProvider,
    //Network,
    //ConnectivityService,
    //AndroidPermissions,
    //Geolocation,
   // LocationAccuracy,
   // CallNumber,
   // GoogleMaps,
   // FCM,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    OneSignal
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
 
}
