import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ExpeditionProvider } from '../api/expedition.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-coursespeciale',
  templateUrl: './coursespeciale.page.html',
  styleUrls: ['./coursespeciale.page.scss'],
})
export class CoursespecialePage implements OnInit {
  listExpedition: any;
  encours: boolean = false
  spinner:boolean=true;
  showcourseencours:boolean=false;
  constructor(private storage: Storage,
    private servicesExpedition: ExpeditionProvider,
    private toastController: ToastController,
    private route: Router) { }

  ngOnInit() {
  /* this.storage.set("coursespeciale", null);
   this.storage.get("coursespeciale").then(val=>{
     console.log(val);
   });*/
  //this.mesaffectation();
  
  }
  ionViewWillEnter(){
    this.mesaffectation();
  }
  mesaffectation() {
   
    this.storage.get("user").then(user => {
      var livreur = {
        "idLivreur": user.id
      }
      this.storage.get("courseSpecialDemarer").then(aff => {
        console.log(aff);
        if (aff != null) {
          this.showcourseencours = true;
         // this.spinner = false
        }else{
       //   this.spinner = false
        }
      })
      this.servicesExpedition.mesaffectionSpeciale(livreur).subscribe(list => {
        this.listExpedition = [];
        if(list){
          this.storage.get("coursespeciale").then(aff => {
            console.log(aff);
            if (aff != null) {
             // this.showcourseencours = true;
              this.spinner = false
              for (let e of list) {
                
                if (e.idAffectationcommerce == aff.idAffectationcommerce) {
                  let index = list.indexOf(e);
                  if (index > -1) {
  
                    list[index] = aff;
  
                    this.encours = true;
                  //  var message: string = 'Vous deja une course en cours.'
                   // this.toast(message);
                  }
  
                }
              }
              this.listExpedition = list;
            }else{
              this.spinner = false
              this.listExpedition = list;
            }
              
              var obj = {
                adresseCommerce: "Route de l'aéroport, Ngor Dakar, Sénégal",
                commerce: "Brioche dorée",
                dateDebut: "12/06/2019",
                etatAffectation: "En attente",
                heureDebut: "08:30",
                heureFin: "12:30",
                idAffectationcommerce: "12",
                idTypetarif: "1",
                montant: "",
                positionCommerce: "14.7494956,-17.5081087"
              }
              var obj1 = {
                adresseCommerce: "Route de l'aéroport, Ngor Dakar, Sénégal",
                commerce: "KING FAHD PALACE",
                dateDebut: "12/06/2019",
                etatAffectation: "En attente",
                heureDebut: "08:30",
                heureFin: "12:30",
                idAffectationcommerce: "13",
                idTypetarif: "1",
                montant: "",
                positionCommerce: "14.7494956,-17.5081087"
              }
              var obj2 = {
                adresseCommerce: "Route de l'aéroport, Ngor Dakar, Sénégal",
                commerce: "CHEZ KATIA",
                dateDebut: "12/06/2019",
                etatAffectation: "En attente",
                heureDebut: "08:30",
                heureFin: "12:30",
                idAffectationcommerce: "14",
                idTypetarif: "1",
                montant: "",
                positionCommerce: "14.7494956,-17.5081087"
              }
              var obj3 = {
                adresseCommerce: "Route de l'aéroport, Ngor Dakar, Sénégal",
                commerce: "LE GONDOLIER",
                dateDebut: "12/06/2019",
                etatAffectation: "En attente",
                heureDebut: "08:30",
                heureFin: "12:30",
                idAffectationcommerce: "15",
                idTypetarif: "1",
                montant: "",
                positionCommerce: "14.7494956,-17.5081087"
              }
              var obj4 = {
                adresseCommerce: "Route de l'aéroport, Ngor Dakar, Sénégal",
                commerce: "KFC ALMADIES",
                dateDebut: "12/06/2019",
                etatAffectation: "En attente",
                heureDebut: "08:30",
                heureFin: "12:30",
                idAffectationcommerce: "18",
                idTypetarif: "1",
                montant: "",
                positionCommerce: "14.7494956,-17.5081087"
              }
              var obj5 = {
                adresseCommerce: "Route de l'aéroport, Ngor Dakar, Sénégal",
                commerce: "Auchan",
                dateDebut: "12/06/2019",
                etatAffectation: "En attente",
                heureDebut: "08:30",
                heureFin: "12:30",
                idAffectationcommerce: "17",
                idTypetarif: "1",
                montant: "",
                positionCommerce: "14.7494956,-17.5081087"
              }
           /*   this.listExpedition.push(obj)
              this.listExpedition.push(obj1)
              this.listExpedition.push(obj2)
              this.listExpedition.push(obj3)
              this.listExpedition.push(obj4)
              this.listExpedition.push(obj5)*/
            
          })
          
        }else{
          this.spinner = false
         
        }

        console.log(list)
      }, err => {
        console.log(err)
      })
    })
  }
  async toast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
  index(item):number {
    var index = this.listExpedition.indexOf(item) + 1;
   /* if(this.listExpedition.indexOf(item)==0){
           index = 1;
    }else{
      index = this.listExpedition.indexOf(item);
    }*/
    return index;
  //  console.log(this.listExpedition.indexOf(item))
  }
  detailsCourse(item:any){
    //this.storage.set("courseSpecialDemarer",null);
    this.storage.get("courseSpecialDemarer").then(course=>{
      if(course){
        if(item.idAffectationcommerce == course.idAffectationcommerce){
           //this.storage.set("coursespeciale",item);
          this.route.navigate(['detailcoursepage']);
        }else{
          this.toast("Une affectation est déjà lancée");
        }
        
      }else{
        this.storage.set("coursespeciale",item);
        this.route.navigate(['detailcoursepage']);
      }
    })
    
  }
  linkDetailCourse(){
    this.route.navigate(['detailcoursepage']);
  }

}
