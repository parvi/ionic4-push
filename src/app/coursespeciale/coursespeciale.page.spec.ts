import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursespecialePage } from './coursespeciale.page';

describe('CoursespecialePage', () => {
  let component: CoursespecialePage;
  let fixture: ComponentFixture<CoursespecialePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursespecialePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursespecialePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
