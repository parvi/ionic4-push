import { Component } from '@angular/core';

import {  Router } from '@angular/router';
import { AlertController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { ExpeditionProvider } from '../api/expedition.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  pseudo:string="";
  locationCoords: any;
  timetest: any;
  syn:boolean=false;
  livreur:any={
    nom:"",
    telephone:"",
    email:""
  };
  listExpedition:any
  nbAff:number=0;
  nbAffS:number=0;
  constructor(
    private router: Router,public alertController: AlertController,
    private storage: Storage,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    private platform: Platform,
    private servicesExpedition: ExpeditionProvider
    ){

      //this.storage.set("coursencours", null);
     // this.storage.set("etat", null);
     //this.storage.set("coursespeciale", null);

      this.mesaffectation();
      this.mesaffectationSpeciale();
    
      this.storage.get("user").then((user)=>{
         this.pseudo = user.nom;
      })
      this.storage.get("user").then
      (user=>{
        console.log(user)
        this.livreur = user
      })
      this.locationCoords = {
        latitude: "",
        longitude: "",
        accuracy: "",
        timestamp: ""
      }
      this.timetest = Date.now();
      if (this.platform.is('cordova')) {
        this.checkGPSPermission();
          } else {
            
          }
    }

  expeditionPage(){
    this.storage.get("affectation").then( async aff=>{
      if(aff){
        this.presentAlertConfirm();
       
      }else{
       const alert = await this.alertController.create({
         header: 'Information',
         message: '<Strong><p>Veuillez d\'abords séléctionner une course sur la liste</p></Strong>',
         buttons: [
           {
             text: 'Annuler',
             role: 'cancel',
             cssClass: 'secondary',
             handler: () => {
           
             }
           }, {
             text: 'D\'accord',
             handler: (data) => {
               this.router.navigate(['affectation']);
 
             }
           }
         ]
       });
 
       await alert.present();
      }
    })
  

    
  }
  comptePage(){
    this.router.navigate(['compte']);
  }
  mescoursesPage(){
    this.router.navigate(['coursespeciale']);
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Confirmation',
    
      message: '<strong "><p>Vous avez actuellement une course en cours</p></strong>',
      buttons: [
        {
          text: 'Poursuivre la course',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.router.navigate(['homme']);
          }
        }, {
          text: 'Voire la liste',
          handler: () => {
            this.router.navigate(['affectation']);
          }
        }
      ]
    });

    await alert.present();
  }
  //Check if application having GPS access permission  
  checkGPSPermission() {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
      result => {
        if (result.hasPermission) {
 
          //If having permission show 'Turn On GPS' dialogue
          this.askToTurnOnGPS();
        } else {
 
          //If not having permission ask for permission
          this.requestGPSPermission();
        }
      },
      err => {
        alert(err);
      }
    );
  }
 
  requestGPSPermission() {
    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      if (canRequest) {
        console.log("4");
      } else {
        //Show 'GPS Permission Request' dialogue
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
          .then(
            () => {
              // call method to turn on GPS
              this.askToTurnOnGPS();
            },
            error => {
              //Show alert if user click on 'No Thanks'
              alert('requestPermission Error requesting location permissions ' + error)
            }
          );
      }
    });
  }
 
  askToTurnOnGPS() {
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => {
        // When GPS Turned ON call method to get Accurate location coordinates
        this.getLocationCoordinates()
      },
      error => alert('Error requesting location permissions ' + JSON.stringify(error))
    );
  }
 
  // Methos to get device accurate coordinates using device GPS
  getLocationCoordinates() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.locationCoords.latitude = resp.coords.latitude;
      this.locationCoords.longitude = resp.coords.longitude;
      this.locationCoords.accuracy = resp.coords.accuracy;
      this.locationCoords.timestamp = resp.timestamp;
      
    }).catch((error) => {
      alert('Error getting location' + error);
    });
  }
 
  deconnexion(){
  this.storage.set("user",null);
    this.router.navigate(['connexion']);
 //   this.router.navigate(['affectation']);
    
  } 
  async sync(){
    this.mesaffectation();
    this.mesaffectationSpeciale();  
  }

  async mesaffectation() {
    /*this.storage.set("affectation", null);
    this.storage.set("coursencours", null);
    this.storage.set("etat", null)*/
    //this.coursesEncours();
    var value = {
      "idLivreur": 18
    }
    this.storage.get("user").then(async user => {
      var livreur = {
        "idLivreur": user.id
      }
      this.servicesExpedition.mesaffectionExpedition(livreur).subscribe(list => {
        console.log(list)
        this.listExpedition = [];
        if (list) {
          this.nbAff = list.length;
         // this.showlist = false;
        /*  this.storage.get("affectation").then(aff => {
            if (aff != null) {
              for (let e of list) {
                if (e.idDemande == aff.idDemande) {
                  let index = list.indexOf(e);
                  if (index > -1) {

                    list[index] = aff;

                    this.encours = true;
                    var message: string = 'Vous deja une course en cours.'
                    this.toast(message);
                  }

                }
              }
            }
          })*/
          this.listExpedition = list;
          
        } else {
         
        }

        // console.log(list)
      }, err => {
        console.log(err)
      })
    })
  }

  mesaffectationSpeciale() {
   
    this.storage.get("user").then(user => {
      var livreur = {
        "idLivreur": user.id
      }
    /*  this.storage.get("courseSpecialDemarer").then(aff => {
        console.log(aff);
        if (aff != null) {
          this.showcourseencours = true;
        }else{
     
        }
      })*/
      this.servicesExpedition.mesaffectionSpeciale(livreur).subscribe(list => {
        this.listExpedition = [];
        if(list){
         // this.storage.get("coursespeciale").then(aff => {
           // console.log(aff);
        //    if (aff != null) {
             // this.showcourseencours = true;
             // this.spinner = false
           /*   for (let e of list) {
                
                if (e.idAffectationcommerce == aff.idAffectationcommerce) {
                  let index = list.indexOf(e);
                  if (index > -1) {
  
                    list[index] = aff;
  
                    this.encours = true;
                  //  var message: string = 'Vous deja une course en cours.'
                   // this.toast(message);
                  }
  
                }
              }*/
              this.nbAffS = list.length
              this.listExpedition = list;
         //   }else{
            /*  this.spinner = false
              this.listExpedition = list;*/
            //}
              
            
         // })
          
        }else{
      //    this.spinner = false
         
        }

        console.log(list)
      }, err => {
        console.log(err)
      })
    })
  }
}