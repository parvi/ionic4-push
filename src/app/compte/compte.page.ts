import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.page.html',
  styleUrls: ['./compte.page.scss'],
})
export class ComptePage implements OnInit {
   livreur:any={
     nom:"",
     telephone:"",
     email:""
   }
  constructor(private storage: Storage, private router: Router) {
    this.storage.get("user").then
      (user=>{
        this.livreur = user
      })
  }

  ngOnInit() {
  }
  deconnexion(){
    this.storage.set("user",null);
    this.router.navigate(['connexion']);
  }
}
