import { Injectable } from '@angular/core';

import { MainservicesService } from '../mainservices.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ExpeditionProvider {

  constructor(public services: MainservicesService, private http:HttpClient ) {
}
mesaffectionExpedition(values:any):any{
//return this.http.post<any>(this.services.apiUrl+"/demande/listeCourseLivreur.php",values);
return this.http.post<any>(this.services.apiUrl+"/demande/listeCourseLivreur.php",values);

}
mesaffectionSpeciale(values:any):any{
  return this.http.post<any>(this.services.apiUrl+"/commerce/listeAffectationCommerce.php",values);
  }
updateAffectation(values:any){
    return this.http.post<any>(this.services.apiUrl+"/demande/setEtatCourse.php",values);
}
optionAffectation(values){
  ///commerce/setEtatAffectation.php
  return this.http.post<any>(this.services.apiUrl+"/commerce/setEtatAffectation.php",values);
}
ajoutExoedition(values){
 // /expedition/ajoutLivreur.php
 return this.http.post<any>(this.services.apiUrl+"/expedition/ajoutLivreur.php",values);
}
monterAverser(values){
  ///demande/montantAverserLivreur.php
  return this.http.post<any>(this.services.apiUrl+"/demande/montantAverserLivreur.php",values);
}
envoyerTracking(values){
  return this.http.post<any>(this.services.apiUrl+"/livreur/trackingCourse.php",values);
}

}
