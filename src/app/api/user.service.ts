import { Injectable } from '@angular/core';

import { catchError, tap, map } from 'rxjs/operators';
import { Observable, of, throwError } from  'rxjs';
import { MainservicesService } from '../mainservices.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
              public services: MainservicesService, private http:HttpClient ) {

               }
connexion(values:any):any{
return this.http.post<any>(this.services.apiUrl+"/utilisateur/connexion.php",values);
}
sendTokenFireBase(values){
  return this.http.post<any>(this.services.apiUrl+"/utilisateur/setToken.php",values);
}
}
