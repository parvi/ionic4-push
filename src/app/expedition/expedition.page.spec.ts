import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpeditionPage } from './expedition.page';

describe('ExpeditionPage', () => {
  let component: ExpeditionPage;
  let fixture: ComponentFixture<ExpeditionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpeditionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpeditionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
