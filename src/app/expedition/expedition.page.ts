import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ExpeditionProvider } from '../api/expedition.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-expedition',
  templateUrl: './expedition.page.html',
  styleUrls: ['./expedition.page.scss'],
})
export class ExpeditionPage implements OnInit {
  nometat: boolean = false;
  telephoneetat: boolean = false;
  emailetat: boolean = false;
  currentLat: any = null;
  currentLng: any = null;
  marker: any
  icon: string;
  nom: string = '';
  telephone: string = '';
  adresse: string = '';
  adresseExpediteur: string = '';
  positionNull: boolean = false;
  idTypetarif: number;
  montant: number = null;
  montantetat: boolean = false;
  locationCoords: any;
  constructor(private storage: Storage, 
    private serviceExpedition: ExpeditionProvider,
    private androidPermissions: AndroidPermissions,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    private loadingController: LoadingController,
    private route: Router) { 
     // this.checkGPSPermission();
     this.locationCoords = {
      latitude: "",
      longitude: "",
      accuracy: "",
      timestamp: ""
    }
     this.getLocationCoordinates();
    }

  ngOnInit() {
    this.storage.get("coursespeciale").then(course => {
      console.log(course)
      this.adresseExpediteur = course.adresseCommerce
      this.idTypetarif = course.idTypetarif;
      /* this.commerce = course.commerce
         this.dateDebut=course.dateDebut
         this.heureDebut     =course.heureDebut
         this.heureFin         =course.heureFin
         this.idAffectationcommerce=course.idAffectationcommerce
         this.idTypetarif=course.idTypetarif
         this.montant=course.montant
         this.positionCommerce = course.positionCommerce*/
    })
  }
  hidemessage() {
    this.nometat = false;
    this.telephoneetat = false;
    this.emailetat = false;
    this.montantetat = false;
  }
  initVar() {
    this.currentLat = null;
    this.currentLng = null;
    this.adresseExpediteur = '';
    this.telephone = '';
    this.nom = '';
    this.adresse = '';
    this.montant = 0;
  }
  searchDestin() {
    var value = {
      telephone: this.telephone
    }
    console.log(value)
    /*  this.serviceExpedition.rechercheClient(value)
      .subscribe(response=>{
        console.log(response);
      })*/
  }
  async validerExpedition() {
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
      message: '...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    loading.present();
    if (this.telephone == '') {
      this.telephoneetat = true;
      loading.dismiss()
    } else if (this.nom == '') {
      this.nometat = true;
      loading.dismiss()
    } else if (this.adresse == '') {
      this.emailetat = true;
      loading.dismiss()
    } else if (this.idTypetarif == 1) {
      if (this.montant == null) {
        loading.dismiss()
        this.montantetat = true;
      }else{
        this.storage.get("coursespeciale").then(course => {
          this.storage.get("user").then(user => {
            var value = {
              position:"position",
              adresse:this.adresseExpediteur,
              nom:this.nom,
              telephone:this.telephone,
              adresseexp:this.adresseExpediteur,
              idLivreur:user.id,
              montantCourse:this.montant,
              latitudeLivreur:this.locationCoords.latitude,
              longitudeLivreur:this.locationCoords.longitude,
              idAffectationcommerce:course.idAffectationcommerce,
              idExpediteur:user.id
              }
              this.serviceExpedition.optionAffectation(value).subscribe(reponse => {
                loading.dismiss()
                console.log(reponse);
                this.storage.get("nbExpCommerce").then(nb=>{
                  if(nb){
                    nb = nb+1;
                    this.storage.set("nbExpCommerce",nb);
                  }else{
                    this.storage.set("nbExpCommerce",1);
                  }
                  this.route.navigate(['detailcoursepage']);
                },err=>{
                  loading.dismiss()
                });
              },err=>{
                loading.dismiss()
              })
          },err=>{
            loading.dismiss()
          })
        },err=>{
          loading.dismiss()
        })
      }
    }else {
      this.storage.get("coursespeciale").then(course => {
        this.storage.get("user").then(user => {
          var value = {
            position:"position",
            adresse:this.adresseExpediteur,
            nom:this.nom,
            telephone:this.telephone,
            adresseexp:this.adresseExpediteur,
            idLivreur:user.id,
            montantCourse:this.montant,
            latitudeLivreur:this.locationCoords.latitude,
            longitudeLivreur:this.locationCoords.longitude,
            idAffectationcommerce:course.idAffectationcommerce,
            idExpediteur:user.id
            }
            this.serviceExpedition.optionAffectation(value).subscribe(reponse => {
              loading.dismiss()
              console.log(reponse);
              this.storage.get("nbExpCommerce").then(nb=>{
                if(nb){
                  nb = nb+1;
                  this.storage.set("nbExpCommerce",nb);
                }else{
                  this.storage.set("nbExpCommerce",1);
                }
                this.route.navigate(['detailcoursepage']);
              },err=>{
                loading.dismiss()
              });
            },err=>{
              loading.dismiss()
            })
        },err=>{
          loading.dismiss()
        })
      },err=>{
        loading.dismiss()
      })
     
    }
  }

     //Check if application having GPS access permission  
     checkGPSPermission() {
      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
        result => {
          if (result.hasPermission) {
   
            //If having permission show 'Turn On GPS' dialogue
            this.askToTurnOnGPS();
          } else {
   
            //If not having permission ask for permission
            this.requestGPSPermission();
          }
        },
        err => {
          alert(err);
        }
      );
    }
   
    requestGPSPermission() {
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {
        if (canRequest) {
          console.log("4");
        } else {
          //Show 'GPS Permission Request' dialogue
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
            .then(
              () => {
                // call method to turn on GPS
                this.askToTurnOnGPS();
              },
              error => {
                //Show alert if user click on 'No Thanks'
                alert('requestPermission Error requesting location permissions ' + error)
              }
            );
        }
      });
    }
   
    askToTurnOnGPS() {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          // When GPS Turned ON call method to get Accurate location coordinates
          this.getLocationCoordinates()
        },
        error => alert('Error requesting location permissions ' + JSON.stringify(error))
      );
    }
   
    // Methos to get device accurate coordinates using device GPS
    getLocationCoordinates() {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.locationCoords.latitude = resp.coords.latitude;
        this.locationCoords.longitude = resp.coords.longitude;
        this.locationCoords.accuracy = resp.coords.accuracy;
        this.locationCoords.timestamp = resp.timestamp;
        
      }).catch((error) => {
        alert('Error getting location' + error);
      });
    }
}
