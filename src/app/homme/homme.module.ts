import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { HommePage } from './homme.page';
import { GoogleMaps } from '@ionic-native/google-maps';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { MainservicesService } from '../mainservices.service';
import { UserService } from '../api/user.service';
import { ExpeditionProvider } from '../api/expedition.service';

const routes: Routes = [
  {
    path: '',
    component: HommePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [HommePage],
  providers: [
    GoogleMaps,
    CallNumber,
    AndroidPermissions,
    Geolocation,
    LocationAccuracy,
    MainservicesService,
    UserService,
    ExpeditionProvider,
  ],
})
export class HommePageModule {}
