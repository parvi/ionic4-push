import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HommePage } from './homme.page';

describe('HommePage', () => {
  let component: HommePage;
  let fixture: ComponentFixture<HommePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HommePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HommePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
