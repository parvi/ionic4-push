import { Component, ViewChild, ElementRef } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreCollection
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Plugins } from '@capacitor/core';
import { Platform, AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ExpeditionProvider } from '../api/expedition.service';

import { CallNumber } from '@ionic-native/call-number/ngx';
import { Router } from '@angular/router';
const { Geolocation } = Plugins;

declare var google;

@Component({
  selector: 'app-homme',
  templateUrl: 'homme.page.html',
  styleUrls: ['homme.page.scss']
})
export class HommePage {
  // Firebase Data
  locations: Observable<any>;
  locationsCollection: AngularFirestoreCollection<any>;

  // Map relateda
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  markers = [];

  // Misc
  isTracking = false;
  watch: string;
  user = null;
  currentLat: any;
  currentLng: string;
  trajet: any
  affectation: any;
  isReceived: boolean = false;
  isClosed: boolean = false;
  destination: string;
  icon: string;
  marker: any;
  coordonneesDestination: any;
  directionsDisplay: any;
  message: string = "";
  depart: string;
  locationCoords: any;
  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore,
    private platform: Platform, private storage: Storage, private alertController: AlertController,
    private servicesExpedition: ExpeditionProvider,
    private toastController: ToastController,
    private callNumber: CallNumber,
    private router: Router) {
      this.locationCoords = {
        latitude: "",
        longitude: "",
        accuracy: "",
        timestamp: ""
      }
    this.anonLogin();
  }

  ionViewWillEnter() {
    //this.initvar();
    this.loadMap();
  }

  // Perform an anonymous login and load data
  anonLogin() {
    this.afAuth.auth.signInAnonymously().then(res => {
      this.user = res.user;

      this.locationsCollection = this.afs.collection(
        `locations/${this.user.uid}/track`,
        ref => ref.orderBy('timestamp')
      );

      // Make sure we also get the Firebase item ID!
      this.locations = this.locationsCollection.snapshotChanges().pipe(
        map(actions =>
          actions.map(a => {
            const data = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );

      // Update Map marker on every change
      this.locations.subscribe(locations => {
          console.log(locations);
        this.updateMap(locations);
      });
    });
  }

  // Initialize a blank map
  loadMap() {
    this.storage.get("affectation").then(aff => {
      console.log(aff)
      if (aff) {
        this.affectation = aff;
        if (aff.etatCourse == "Démarrée") {
          this.depart = "Ma position actuelle"
          this.destination = aff.libelleExpedition
          this.isReceived = true;
          var coordonneesDes = aff.adresseExpedition.split(',')
          var obj = { lat: coordonneesDes[0], lng: coordonneesDes[1] }
          this.coordonneesDestination = obj;

        } else if (aff.etatCourse == "Colis récupéré") {
          this.depart = "Ma position actuelle"
          this.destination = aff.libelleDestination
          var coordonneesDes = aff.adresseDestination.split(',')
          var obj = { lat: coordonneesDes[0], lng: coordonneesDes[1] }
          this.coordonneesDestination = obj;
          this.isReceived = false;
          this.isClosed = true;
        } else {

        }
        this.getPosition().then((resp) => {
          let bounds = new google.maps.LatLngBounds();
          this.locationCoords.latitude = resp.coords.latitude
          this.locationCoords.longitude = resp.coords.longitude
         console.log(resp.coords.latitude, resp.coords.longitude)
          let latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
          bounds.extend(latLng);
          let mapOptions = {
            center: latLng,
            mapTypeControl: false,
            //   draggable: false,
            // scaleControl: false,
            scrollwheel: false,
            navigationControl: false,
            streetViewControl: false,
            zoom: 15,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };

          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
          this.map.fitBounds(bounds);
          this.map.panToBounds(bounds);
       //   this.addMaker(resp.coords.latitude, resp.coords.longitude, "../../assets/img/marker1.gif", 'Ma position actuelle');
          var origin = { lat: resp.coords.latitude, lng: resp.coords.longitude }
          var latDes: number = +this.coordonneesDestination.lat;
          var longDes: number = +this.coordonneesDestination.lng;
          var destination = { lat: latDes, lng: longDes }
          this.traceRoute(origin, destination);
          this.startTracking();

        });

      }
    })


  }

  // Use Capacitor to track our geolocation
  startTracking() {

    this.isTracking = true;
    this.watch = Geolocation.watchPosition({}, (position, err) => {
      if (position) {
        this.addNewLocation(
          position.coords.latitude,
          position.coords.longitude,
          position.timestamp
        );
      }
    });
  }

  // Unsubscribe from the geolocation watch using the initial ID
  stopTracking() {
    Geolocation.clearWatch({ id: this.watch }).then(() => {
      this.isTracking = false;
    });
  }

  // Save a new location to Firebase and center the map
  addNewLocation(lat, lng, timestamp) {
    var origin = { lat: lat, lng: lng }
    var latDes: number = +this.coordonneesDestination.lat;
    var longDes: number = +this.coordonneesDestination.lng;
    var destination = { lat: latDes, lng: longDes }
    this.calculate(origin, destination,lat,lng,timestamp);
   // console.log(this.trajet);
   
  //  console.log("envoyer")
    let position = new google.maps.LatLng(lat, lng);
    this.map.setCenter(position);
    this.map.setZoom(15);
    
    
   
  }

  // Delete a location from Firebase
  deleteLocation() {
  //  console.log(this.locations);
  //  console.log(pos)
    this.locations.subscribe(locations => {
      for(let lc of locations){
        this.locationsCollection.doc(lc.id).delete();
      }
      
  });
   // this.locationsCollection.doc(pos.id).delete();
  }

  // Redraw all markers on the map
  updateMap(locations) {
    // Remove all current marker
    this.markers.map(marker => marker.setMap(null));
    this.markers = [];
    this.icon = "../../assets/img/marker1.gif";
    for (let loc of locations) {
      // let bounds  = new google.maps.LatLngBounds();
      let latLng = new google.maps.LatLng(loc.lat, loc.lng);
      //  bounds.extend(latLng);
      //  this.map.fitBounds(bounds);
      // this.map.panToBounds(bounds);
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: latLng,
        icon: this.icon
      });
      this.markers.push(marker);
    }

  }

  getPosition(): any {
      return Geolocation.getCurrentPosition(); 
  }
          
  async calculate(origin: any, destination: any, lat:any, lng:any,timestamp:any) {

    if (origin && destination) {
      var directionsService = new google.maps.DirectionsService();
      var directionsDisplay = new google.maps.DirectionsRenderer(); // Service de calcul d'itinéraire
      directionsService.route({

        origin: origin,

        destination: destination,
        travelMode: google.maps.DirectionsTravelMode.DRIVING

      }, (res, status) => {

        if (status == google.maps.DirectionsStatus.OK) {
          // console.log(res)
          directionsDisplay.setDirections(res);
          this.trajet = res.routes[0].legs[0].distance.text + " / "
            + res.routes[0].legs[0].duration.text;
             console.log(this.trajet);
             var estimation = this.trajet;
             if(lat!=null || lng!=null || timestamp!=null){
              this.locationsCollection.add({
                lat,
                lng,
                timestamp,
                estimation
              });
             }
            
          // this.startTracking();
        } else {
          console.warn(status);
        }

      });

    } //http://code.google.com/intl/fr-FR/apis/maps/documentation/javascript/reference.html#DirectionsRequest
  }
  show(label: string) {
    this.trajet = label;
  }
  initMap() {

  }
  addMaker(lat: any, lng: any, mark: string, text: string) {
    this.icon = mark;
    var infowindow = new google.maps.InfoWindow();

    var i;
    this.marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat, lng),
      map: this.map,
      icon: this.icon
    });

    google.maps.event.addListener(this.marker, 'click', (function (marker, i) {
      return function () {
        infowindow.setContent(text);
        infowindow.open(this.map, marker);
      }
    })(this.marker, i));
  }
  traceRoute(origin: any, destination: any) {
       console.log(origin)
       console.log(destination)
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer(); // Service de calcul d'itinéraire
    directionsDisplay.setMap(this.map);
    directionsDisplay.setOptions({ suppressMarkers: true });
    directionsService.route({
      origin: origin,
      destination: destination,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    }, (res, status) => {
      if (status == google.maps.DirectionsStatus.OK) {
        directionsDisplay.setDirections(res);
     //   this.calculate(origin, destination);
        this.addMaker(destination.lat, destination.lng, "../../assets/img/arrivee.png", 'Destination')
      } else {
        console.warn(status);
      }

    });
  }
  //===========================================================================================================
  //===========================================================================================================
  async recupererPaquet() {

    var message: string = "Je confirme avoir recupérer le paquet!";

    this.confirmationMessageOptions(message, "recuperer");

  }

  async confirmationMessageOptions(message: string, etat: string) {
    const alert = await this.alertController.create({
      header: 'Info',
      message: '<strong><p>' + message + '</p></strong>',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // this.router.navigate(['affectation']);
          }
        }, {
          text: 'D\'accord',
          handler: () => {
            // this.progress = 0.5;
            this.storage.get("affectation").then
              (object => {
                console.log(object)
                console.log(etat)
                if (etat == "terminer") {
                 //Traitement cloturer course
                  //Recuperation des coordonnees
                //  this.getPosition().then((resp) => {
                   
                    var terminer = { idDemande: object.idDemande, etat: 3, latitude: this.locationCoords.latitude, longitude: this.locationCoords.longitude }
                    //Envoi des donnees pour la mis a jour
                    this.servicesExpedition.updateAffectation(terminer).subscribe
                      (response => {
                        console.log(response)
                        if (response.response == 1) {
                         
                          this.storage.set("affectation", null)
                          this.storage.set("etat", null);

                          //Mis a jour de la carte map
                          let bounds = new google.maps.LatLngBounds();
                          console.log(this.currentLat + " " + this.currentLng)
                          let latLng = new google.maps.LatLng(this.locationCoords.latitude, this.locationCoords.longitude);
                          bounds.extend(latLng);
                          let mapOptions = {
                            center: latLng,
                            mapTypeControl: false,
                            scrollwheel: false,
                            navigationControl: false,
                            streetViewControl: false,
                            zoom: 15,
                            disableDefaultUI: true,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                          };
                          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
                          this.map.fitBounds(bounds);
                          this.map.panToBounds(bounds);
                          this.addMaker(this.locationCoords.latitude, this.locationCoords.longitude, "../../assets/img/marker1.gif", 'Ma position actuelle');
                          
                          //directionsDisplay.setMap(this.map);
                          this.isClosed = false;
                          this.alertCloture();  
                         /* var message: string = 'Course cloturée'
                          this.toast(message);*/
                          
                        } else {
                          var message: string = 'Erreur lors du traitement!'
                          this.toast(message);
                        }

                   //   });
                    //Fin bloc location
                  });
    
                } else if (etat == "recuperer") {
                  //Traitement recuperation paquet
                  //Recuperation des coordonnees
                //  this.getPosition().then((resp) => {
                   // console.log(resp)
                  
                    var recuperer = { idDemande: object.idDemande, etat: 2, latitude: this.locationCoords.latitude, longitude: this.locationCoords.longitude }
                    //Envoi des donnees pour la mis a jour
                   
                    this.servicesExpedition.updateAffectation(recuperer).subscribe
                      (response => {
                        console.log(response)
                        if (response.response == 1) {
                          //Preparation de l objet apres mis ajour
                          var val = {
                            idDemande: object.idDemande,
                            adresseExpedition: object.adresseExpedition,
                            adresseDestination: object.adresseDestination,
                            libelleExpedition: object.libelleExpedition,
                            libelleDestination: object.libelleDestination,
                            telephoneExpediteur: object.telephoneExpediteur,
                            telephoneDestinataire: object.telephoneExpediteur,
                            expediteur: object.expediteur,
                            destinataire: object.destinataire,
                            etatCourse: "Colis récupéré"
                          }

                          console.log(object.etatCourse)
                          this.storage.set("affectation", val)
                          this.storage.set("etat", 2);

                          //Mis a jour de la carte map
                          let bounds = new google.maps.LatLngBounds();
                          console.log(this.currentLat + " " + this.currentLng)
                          let latLng = new google.maps.LatLng(this.locationCoords.latitude, this.locationCoords.longitude);
                          bounds.extend(latLng);
                          let mapOptions = {
                            center: latLng,
                            mapTypeControl: false,
                            scrollwheel: false,
                            navigationControl: false,
                            streetViewControl: false,
                            zoom: 15,
                            disableDefaultUI: true,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                          };
                          var origin = { lat: this.locationCoords.latitude, lng: this.locationCoords.longitude }
                          this.depart = "Ma position actuelle"
                          this.destination = object.libelleDestination
                          var coordonneesDes = object.adresseDestination.split(',')
                          var obj = { lat: coordonneesDes[0], lng: coordonneesDes[1] }
                          this.coordonneesDestination = obj;

                          var latDes: number = +this.coordonneesDestination.lat;
                          var longDes: number = +this.coordonneesDestination.lng;
                          var destination = { lat: latDes, lng: longDes }
                          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
                          this.map.fitBounds(bounds);
                          this.map.panToBounds(bounds);
                          this.addMaker(this.locationCoords.latitude, this.locationCoords.longitude, "../../assets/img/marker1.gif", 'Ma position actuelle');
                          var directionsService = new google.maps.DirectionsService();
                          var directionsDisplay = new google.maps.DirectionsRenderer();
                          directionsDisplay.setMap(this.map); // Service de calcul d'itinéraire
                          //directionsDisplay.setMap(this.map);
                          directionsDisplay.setOptions({ suppressMarkers: true });
                          directionsService.route({
                            origin: origin,
                            destination: destination,
                            travelMode: google.maps.DirectionsTravelMode.DRIVING
                          }, (res, status) => {
                            if (status == google.maps.DirectionsStatus.OK) {
                              directionsDisplay.setDirections(res);
                             // this.calculate(origin, destination);
                              this.addMaker(destination.lat, destination.lng, "../../assets/img/arrivee.png", 'Destination')
                            } else {
                              console.warn(status);
                            }

                          });
                          this.isReceived = false;
                          this.isClosed = true;
                          var message: string = 'Colis récupéré'
                          this.toast(message);
                        } else {
                          var message: string = 'Erreur lors du traitement!'
                          this.toast(message);
                        }

                    //  });
                    //Fin bloc location
                  });

                } else {

                }
              })
          }

        }
      ]
    });

    await alert.present();
  }
  async toast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }
  getLocationCoordinates() {
    console.log(
      this.locationCoords.latitude,
      this.locationCoords.longitude
    );
   
  }
  initvar() {
    this.storage.get("affectation").then(aff => {
      console.log(aff)
      if (aff) {
        this.affectation = aff;
        if (aff.etatCourse == "Démarrée") {
          this.destination = aff.libelleExpedition
          this.isReceived = true;
          var coordonneesDes = aff.adresseExpedition.split(',')
          var obj = { lat: coordonneesDes[0], lng: coordonneesDes[1] }
          this.coordonneesDestination = obj;

        } else if (aff.etatCourse == "Colis récupéré") {

          this.destination = aff.libelleDestination
          var coordonneesDes = aff.adresseDestination.split(',')
          var obj = { lat: coordonneesDes[0], lng: coordonneesDes[1] }
          this.coordonneesDestination = obj;
          this.isReceived = false;
          this.isClosed = true;
        } else {

        }
      }

    })

  }
  async appelleExpediteurOrDestinataire() {
    this.storage.get("affectation").then(async aff => {
      console.log()
      const alert = await this.alertController.create({
        header: 'Appel',
        message: '<Strong><p>Veuillez choisir un numéro pour appeler</p></Strong>' +
          '<p>(1) Expediteur. ' + aff.telephoneExpediteur + '</p>' +
          '<p>(2) Destinataire. ' + aff.telephoneDestinataire + '</p>',
        inputs: [
          {
            name: 'name1',
            type: 'number',
            placeholder: ''
          }
        ],
        buttons: [
          {
            text: 'Annuler',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          }, {
            text: 'Appeler',
            handler: (data) => {
              if (data.name1 != "") {
                if (data.name1 == "1") {
                  this.appeller(aff.telephoneExpediteur);
                } else {
                  this.appeller(aff.telephoneDestinataire);
                }
                console.log(data);
              } else {
                console.log("erreur")
              }

            }
          }
        ]
      });

      await alert.present();
    })

  }

  async alertCloture(){
    const alert = await this.alertController.create({
      header: 'Information',
      message: '<Strong><p>Vous avez cloturé la course</p></Strong>',
      buttons: [
        {
          text: 'D\'accords',
          handler: (data) => {
            this.deleteLocation();  
            this.router.navigate(['home'])

          }
        }
      ]
    });

    await alert.present();

  }
  async cloturerCourse() {
    this.storage.get("etat").then((paquet) => {
      if (paquet != null) {
        if (paquet == 2) {
          var message: string = "Je confirme avoir terminer la course!";
          this.confirmationMessageOptions(message, "terminer");
        } else {
          var message: string = "Vous ne pouvez pas cloturer la course actuellement";
          this.confirmationMessageOptions(message, "no");
        }
      } else {
        var message: string = "Vous ne pouvez pas cloturer la course actuellement";
        this.confirmationMessageOptions(message, "no");
      }
    })
  }
  appeller(tel: string) {
    this.callNumber.callNumber(tel, true)
      .then(res => { })
      .catch(err => { });
  }
}
