import { Component } from '@angular/core';
import { Platform, MenuController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';
import { OneSignal } from '@ionic-native/onesignal/ngx';

const config = {
  apiKey: "AIzaSyBAA6qZUTTFWFDYoO-KrH2NHpbj-qNV42g",
  authDomain: "dht-htsoft.firebaseapp.com",
  databaseURL: "https://dht-htsoft.firebaseio.com",
  projectId: "dht-htsoft",
  storageBucket: "gs://dht-htsoft.appspot.com/",
  messagingSenderId: "133539155181"
};
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Accueil',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Profile',
      url: '/compte',
      icon: 'person'
    },
    {
      title: 'Affectation',
      url: '/affectation',
      icon: 'star-outline'
    },
    
    {
      title: 'Affectation spéciale',
      url: '/coursespeciale',
      icon: 'star'
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private storage: Storage,
    private menuCtrl: MenuController,
    private oneSignal: OneSignal,
    private alertCtrl: AlertController
 
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.rootPage();
      firebase.initializeApp(config);
      this.setupPush();
    });
  }

     

  rootPage(){
    this.storage.get("user").then((user)=>{
      console.log(user)
      if(user!=null){
        this.router.navigate(['home']);
      }else{
        this.router.navigate(['connexion']);
      }
    })
    
  }
  setupPush() {
    // I recommend to put these into your environment.ts
    this.oneSignal.startInit('209a288c-46ca-4e84-a4af-ae697e35b026', 'YOUR ANDROID ID');
 
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
 
    // Notifcation was received in general
    this.oneSignal.handleNotificationReceived().subscribe(data => {
      let msg = data.payload.body;
      let title = data.payload.title;
      let additionalData = data.payload.additionalData;
      this.confirmationMessageOptions(title, msg, additionalData.task);
    });
 
    // Notification was really clicked/opened
    this.oneSignal.handleNotificationOpened().subscribe(data => {
      // Just a note that the data is a different place here!
      let additionalData = data.notification.payload.additionalData;
 
      this.confirmationMessageOptions('Notification opened', 'You already read this before', additionalData.task);
    });
 
    this.oneSignal.endInit();
  }
 
  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          }
        }
      ]
    })
    alert.present();
  }
  async confirmationMessageOptions(title, msg, task) {
    
    const alert = await this.alertCtrl.create({
      header: ''+title,
      message: '<strong><p>' + msg + '</p></strong>',
      buttons: [
        {
          text: 'OK'+task,
          handler: () => {
           
          }
        }
      ]
    });

    await alert.present();
  }
 /*
telephone:"781586996",
  motDePasse:"123"*/

}
