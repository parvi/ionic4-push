import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpeditioncommercialePage } from './expeditioncommerciale.page';

describe('ExpeditioncommercialePage', () => {
  let component: ExpeditioncommercialePage;
  let fixture: ComponentFixture<ExpeditioncommercialePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpeditioncommercialePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpeditioncommercialePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
