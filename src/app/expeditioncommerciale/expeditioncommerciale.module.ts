import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExpeditioncommercialePage } from './expeditioncommerciale.page';

const routes: Routes = [
  {
    path: '',
    component: ExpeditioncommercialePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ExpeditioncommercialePage]
})
export class ExpeditioncommercialePageModule {}
