// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBAA6qZUTTFWFDYoO-KrH2NHpbj-qNV42g",
    authDomain: "dht-htsoft.firebaseapp.com",
    databaseURL: "https://dht-htsoft.firebaseio.com",
    projectId: "dht-htsoft",
    storageBucket: "gs://dht-htsoft.appspot.com/",
    messagingSenderId: "133539155181"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
